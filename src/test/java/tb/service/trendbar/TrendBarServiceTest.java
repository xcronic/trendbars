package tb.service.trendbar;

import com.google.common.collect.ImmutableList;
import org.assertj.core.groups.Tuple;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import tb.config.TrendBarConfiguration;
import tb.mapper.trendbar.TrendBarMapper;
import tb.model.Quote;
import tb.model.Symbol;
import tb.model.TrendBar;
import tb.model.TrendBarPeriod;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TrendBarConfiguration.class, loader = AnnotationConfigContextLoader.class)
@TestPropertySource(properties = {"trendbars.Second.support=true"})
public class TrendBarServiceTest {
    @Autowired
    private TrendBarService trendBarService;
    @Autowired
    @SuppressWarnings("SpringJavaAutowiringInspection")
    private TrendBarMapper trendBarMapper;

    @Test
    @Repeat(10)
    public void testTrendBarService() throws InterruptedException {
        int priceCounter;
        for (priceCounter = 1; priceCounter < 10000; priceCounter++) {
            final Quote quote = new Quote(priceCounter, Symbol.EURUSD, Instant.now());
            trendBarService.updateQuote(quote);
        }

        Thread.sleep(1000);

        final List<TrendBar> trendBars = trendBarService.listTrendBars(
                Symbol.EURUSD,
                TrendBarPeriod.Second,
                Instant.now().minus(10, ChronoUnit.MINUTES),
                Instant.now()
        );

        assertThat(trendBars)
                .isNotEmpty()
                .extracting(
                        TrendBar::getSymbol,
                        TrendBar::getPeriod
                )
                .containsOnly(Tuple.tuple(
                        Symbol.EURUSD,
                        TrendBarPeriod.Second
                ));

        assertThat(
                trendBars.stream()
                        .map(TrendBar::getHighPrice)
                        .collect(ImmutableList.toImmutableList())
        ).contains(priceCounter - 1);

        assertThat(
                trendBars.stream()
                        .map(TrendBar::getLowPrice)
                        .collect(ImmutableList.toImmutableList())
        ).contains(1);
    }

    @Test
    public void testTrendBarMapper() {
        trendBarMapper.createTrendBar(
                Symbol.EURUSD,
                TrendBarPeriod.Minute,
                1,
                10,
                100,
                1,
                Timestamp.from(Instant.now().truncatedTo(ChronoUnit.MINUTES))
        );

        final List<TrendBar> trendBars = trendBarService.listTrendBars(
                Symbol.EURUSD,
                TrendBarPeriod.Minute,
                Instant.now().minus(10, ChronoUnit.MINUTES),
                Instant.now()
        );

        assertThat(trendBars).hasSize(1);
        final TrendBar trendBar = trendBars.get(0);
        assertThat(trendBar)
                .extracting(
                        TrendBar::getSymbol,
                        TrendBar::getPeriod,
                        TrendBar::getOpenPrice,
                        TrendBar::getClosePrice,
                        TrendBar::getHighPrice,
                        TrendBar::getLowPrice
                )
                .contains(
                        Symbol.EURUSD,
                        TrendBarPeriod.Minute,
                        1,
                        10,
                        100,
                        1
                );
    }
}
