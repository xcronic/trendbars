package tb.model;

import com.google.common.collect.ImmutableList;

import java.util.Arrays;

import static com.google.common.collect.ImmutableList.toImmutableList;

public enum Symbol {
    EURUSD,
    EURJPY;

    public static final ImmutableList<String> symbols =
            Arrays.stream(Symbol.values())
                    .map(Symbol::name)
                    .collect(toImmutableList());

    public static Symbol fromString(final String value) {
        try {
            return Symbol.valueOf(value);
        } catch (final IllegalArgumentException | NullPointerException e) {
            return null;
        }
    }
}
