package tb.model;

import com.google.common.collect.ImmutableList;

import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Arrays;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static java.time.temporal.ChronoUnit.SECONDS;

public enum TrendBarPeriod {
    Second(SECONDS),
    Minute(ChronoUnit.MINUTES),
    Hour(ChronoUnit.HOURS),
    Day(ChronoUnit.DAYS);

    private final TemporalUnit temporalUnit;

    public static final ImmutableList<String> periods =
            Arrays.stream(TrendBarPeriod.values())
                    .map(TrendBarPeriod::name)
                    .collect(toImmutableList());


    TrendBarPeriod(final TemporalUnit temporalUnit) {
        this.temporalUnit = temporalUnit;
    }

    public static TrendBarPeriod fromChronoUnit(final ChronoUnit temporalUnit) {
        switch (temporalUnit) {
            case SECONDS:
                return Second;
            case MINUTES:
                return Minute;
            case HOURS:
                return Hour;
            case DAYS:
                return Day;
            default:
                throw new IllegalArgumentException(temporalUnit + " is not supported");
        }
    }

    public static TrendBarPeriod fromString(final String value) {
        try {
            return TrendBarPeriod.valueOf(value);
        } catch (final IllegalArgumentException | NullPointerException e) {
            return null;
        }
    }
}
