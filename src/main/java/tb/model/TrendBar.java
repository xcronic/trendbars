package tb.model;

import java.sql.Timestamp;

public class TrendBar {
    private Symbol symbol;
    private TrendBarPeriod period;
    private Timestamp startTime;
    private int openPrice;
    private int closePrice;
    private int highPrice;
    private int lowPrice;

    public Symbol getSymbol() {
        return symbol;
    }

    public TrendBarPeriod getPeriod() {
        return period;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public int getOpenPrice() {
        return openPrice;
    }

    public int getClosePrice() {
        return closePrice;
    }

    public int getHighPrice() {
        return highPrice;
    }

    public int getLowPrice() {
        return lowPrice;
    }

    @Override
    public String toString() {
        return "TrendBar{" +
                "symbol=" + symbol +
                ", period=" + period +
                ", startTime=" + startTime +
                ", openPrice=" + openPrice +
                ", closePrice=" + closePrice +
                ", highPrice=" + highPrice +
                ", lowPrice=" + lowPrice +
                '}';
    }
}
