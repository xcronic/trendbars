package tb.model;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.time.Instant;

public class Quote {
    private final int price;
    private final Symbol symbol;
    private final Instant timestamp;

    private static final Gson gson = new Gson();

    public Quote(final int price, final Symbol symbol, final Instant timestamp) {
        this.price = price;
        this.symbol = symbol;
        this.timestamp = timestamp;
    }

    public static Quote fromJson(final String json) {
        Preconditions.checkNotNull(json, "json required");

        final Instant now = Instant.now();

        final QuoteParam quoteParam = gson.fromJson(json, QuoteParam.class);
        if (quoteParam == null || quoteParam.price == null || quoteParam.symbol == null) {
            throw new JsonParseException("invalid quote param");
        }

        return new Quote(quoteParam.price, quoteParam.symbol, now);
    }

    public int getPrice() {
        return price;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    private static class QuoteParam {
        private Integer price;
        private Symbol symbol;
    }
}
