package tb.controller;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import tb.model.Quote;
import tb.model.Symbol;
import tb.model.TrendBar;
import tb.model.TrendBarPeriod;
import tb.service.trendbar.TrendBarService;

import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.List;

//switched off
//was created by mistake
//can't raise my hand to delete



//@RestController
public class MainController {
    private static final Gson gson = new Gson();
    private static final Logger log = Logger.getLogger(MainController.class);

    private TrendBarService trendBarService;

    @Autowired
    public final void setTrendBarService(final TrendBarService trendBarService) {
        this.trendBarService = trendBarService;
    }

    @GetMapping(
            value = "/trendbars",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<String> listTrendBars(@RequestParam("symbol") final String symbolRaw,
                                                @RequestParam("period") final String trendBarPeriodRaw,
                                                @RequestParam("fromTimestamp") final String fromTimestampRaw,
                                                @RequestParam("toTimestamp") final String toTimestampRaw) {
        final Symbol symbol = Symbol.fromString(symbolRaw);
        if (symbol == null) {
            log.error("invalid symbol " + symbolRaw);
            return ResponseEntity.badRequest().body("parameter 'symbol' is invalid. Valid symbols are: " + Symbol.symbols);
        }

        final TrendBarPeriod trendBarPeriod = TrendBarPeriod.fromString(trendBarPeriodRaw);
        if (trendBarPeriod == null) {
            log.error("invalid trendBarPeriod " + trendBarPeriodRaw);
            return ResponseEntity.badRequest().body("parameter 'period' is invalid. Valid periods are: " + TrendBarPeriod.periods);
        }

        final Instant fromTimestamp;
        final Instant toTimestamp;
        try {
            fromTimestamp = Instant.parse(fromTimestampRaw);
            toTimestamp = Instant.parse(toTimestampRaw);

            if (!fromTimestamp.isBefore(toTimestamp)) {
                throw new DateTimeParseException("from timestamp must be before to timestamp", "", 0);
            }
        } catch (final DateTimeParseException e) {
            log.error("invalid timestamp from " + fromTimestampRaw + " or to " + toTimestampRaw + "\n");
            log.error(e);
            return ResponseEntity.badRequest().body("timestamp is invalid");
        }

        try {
            final List<TrendBar> trendBarList = trendBarService.listTrendBars(symbol, trendBarPeriod, fromTimestamp, toTimestamp);
            return ResponseEntity.ok(gson.toJson(trendBarList));
        } catch (final Exception e) {
            log.error(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }

    }

    @PostMapping(
            value = "/updateQuote",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<String> updateQuote(@RequestBody final String body) {
        final Quote quote;
        try {
            quote = Quote.fromJson(body);
        } catch (final JsonParseException e) {
            return ResponseEntity.badRequest().body("invalid quote\n");
        }

        trendBarService.updateQuote(quote);

        return ResponseEntity.ok(gson.toJson(quote));
    }

}
