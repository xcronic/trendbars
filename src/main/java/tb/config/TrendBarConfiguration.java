package tb.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import tb.service.trendbar.TrendBarService;
import tb.service.trendbar.TrendBarServiceImpl;

import javax.sql.DataSource;

@Configuration
@MapperScan("tb.mapper")
@PropertySource("classpath:/META-INF/application.properties")
public class TrendBarConfiguration {
    static {
        final Logger rootLogger = Logger.getRootLogger();
        rootLogger.setLevel(Level.INFO);
        rootLogger.addAppender(new ConsoleAppender(
                new PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1} - %m%n")));
    }

    public static void main(final String[] args) {
        new AnnotationConfigApplicationContext(TrendBarConfiguration.class);
    }

    @Bean
    TrendBarService trendBarService() {
        return new TrendBarServiceImpl();
    }

    @Bean
    public DataSource dataSource() {
        final SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(org.h2.Driver.class);
        dataSource.setUsername("sa");
        dataSource.setUrl("jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE");
        dataSource.setPassword("");

        final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.execute("DROP TABLE trendbars IF EXISTS");
        jdbcTemplate.execute("CREATE TABLE trendbars(" +
                "id SERIAL, " +
                "symbol VARCHAR(20) NOT NULL, " +
                "period VARCHAR(20) NOT NULL, " +
                "open_price INTEGER NOT NULL, " +
                "close_price INTEGER NOT NULL, " +
                "high_price INTEGER NOT NULL, " +
                "low_price INTEGER NOT NULL, " +
                "startpoint TIMESTAMP NOT NULL" +
                ")");

        return dataSource;
    }

    @Bean//necessary for mybatis
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        final SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(dataSource());
        return sqlSessionFactory.getObject();
    }
}

