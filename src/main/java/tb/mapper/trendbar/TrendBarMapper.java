package tb.mapper.trendbar;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tb.model.Symbol;
import tb.model.TrendBar;
import tb.model.TrendBarPeriod;

import java.sql.Timestamp;
import java.util.List;

public interface TrendBarMapper {

    @Insert("INSERT INTO trendbars (" +
                "symbol, " +
                "period, " +
                "open_price, " +
                "close_price, " +
                "high_price, " +
                "low_price, " +
                "startpoint" +
            ") \n" +
            "VALUES ("+
                "#{symbol}, " +
                "#{period}, " +
                "#{open_price}, " +
                "#{close_price}, " +
                "#{high_price}, " +
                "#{low_price}, " +
                "#{startpoint}" +
            ")")
    void createTrendBar(@Param("symbol") Symbol symbol,
                        @Param("period") TrendBarPeriod period,
                        @Param("open_price") int openPrice,
                        @Param("close_price") int closePrice,
                        @Param("high_price") int highPrice,
                        @Param("low_price") int lowPrice,
                        @Param("startpoint") Timestamp startTime);

    @Select("SELECT " +
                "tb.symbol symbol, " +
                "tb.period period, " +
                "tb.startpoint startTime, " +
                "tb.open_price openPrice, " +
                "tb.close_price closePrice, " +
                "tb.high_price highPrice, " +
                "tb.low_price lowPrice \n" +
            "FROM trendbars tb " +
            "WHERE " +
                "tb.symbol = #{symbol} " +
                    "AND tb.period = #{period} " +
                    "AND tb.startpoint BETWEEN #{from} AND #{to}" +
            "")
    List<TrendBar> listTrendBars(@Param("symbol") final Symbol symbol,
                                 @Param("period") final TrendBarPeriod period,
                                 @Param("from") final Timestamp from,
                                 @Param("to") final Timestamp to);
}
