package tb.service.trendbar;

import tb.model.Quote;
import tb.model.Symbol;
import tb.model.TrendBar;
import tb.model.TrendBarPeriod;

import java.time.Instant;
import java.util.List;

public interface TrendBarService {
    void updateQuote(Quote quote);

    List<TrendBar> listTrendBars(Symbol symbol,
                                 TrendBarPeriod trendBarPeriod,
                                 Instant fromTimestamp,
                                 Instant toTimestamp);
}
