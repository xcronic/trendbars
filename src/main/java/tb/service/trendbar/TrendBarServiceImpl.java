package tb.service.trendbar;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;
import tb.mapper.trendbar.TrendBarMapper;
import tb.model.Quote;
import tb.model.Symbol;
import tb.model.TrendBar;
import tb.model.TrendBarPeriod;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
public class TrendBarServiceImpl implements TrendBarService, InitializingBean, DisposableBean {
    private final Multimap<Instant, Quote> secondQuotes = Multimaps.synchronizedMultimap(ArrayListMultimap.create());
    private final Multimap<Instant, Quote> minuteQuotes = Multimaps.synchronizedMultimap(ArrayListMultimap.create());
    private final Multimap<Instant, Quote> hourQuotes = Multimaps.synchronizedMultimap(ArrayListMultimap.create());
    private final Multimap<Instant, Quote> dayQuotes = Multimaps.synchronizedMultimap(ArrayListMultimap.create());

    private final ImmutableMap<ChronoUnit, Multimap<Instant, Quote>> quotesMap = ImmutableMap.of(
            ChronoUnit.SECONDS, secondQuotes,
            ChronoUnit.MINUTES, minuteQuotes,
            ChronoUnit.HOURS, hourQuotes,
            ChronoUnit.DAYS, dayQuotes
    );

    private final ExecutorService executorService = Executors.newSingleThreadExecutor(runnable -> {
        final Thread thread = new Thread(runnable);
        thread.setDaemon(true);
        thread.setName("quotes_proc");
        return thread;
    });

    private final ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();

    @Autowired
    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    private TrendBarMapper trendBarMapper;

    @Autowired
    @SuppressWarnings("SpringAutowiredFieldsWarningInspection")
    private Environment env;

    private static final Logger log = Logger.getLogger(TrendBarServiceImpl.class);
    private static final Map<ChronoUnit, Long> periodLengthMap = ImmutableMap.of(
            ChronoUnit.SECONDS, 1000L,
            ChronoUnit.MINUTES, 60L * 1000L,
            ChronoUnit.HOURS, 60L * 60L * 1000L,
            ChronoUnit.DAYS, 24L * 60L * 60L * 1000L
    );

    @Override
    public void destroy() throws Exception {
        executorService.shutdown();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        scheduler.initialize();

        scheduleTrendBarPersist(ChronoUnit.SECONDS);
        scheduleTrendBarPersist(ChronoUnit.MINUTES);
        scheduleTrendBarPersist(ChronoUnit.HOURS);
        scheduleTrendBarPersist(ChronoUnit.DAYS);
    }

    private void scheduleTrendBarPersist(final ChronoUnit chronoUnit) {
        if (env.getProperty("trendbars." + TrendBarPeriod.fromChronoUnit(chronoUnit) + ".support", Boolean.class)) {
            scheduler.scheduleAtFixedRate(
                    () -> {
                        log.info(chronoUnit + " trendbar creation started");
                        final Instant previousPeriodStart = Instant.now().truncatedTo(chronoUnit).minus(1, chronoUnit);

                        final Multimap<Instant, Quote> periodQuotes = quotesMap.get(chronoUnit);
                        final Collection<Quote> quotes = periodQuotes.get(previousPeriodStart);
                        if (CollectionUtils.isNotEmpty(quotes)) {
                            saveTrendBar(previousPeriodStart, TrendBarPeriod.fromChronoUnit(chronoUnit), ImmutableList.copyOf(periodQuotes.values()));
                            periodQuotes.removeAll(previousPeriodStart);
                            log.info(chronoUnit + " trendbar creation done");
                        }
                    },
                    Date.from(Instant.now().truncatedTo(chronoUnit).plus(1, chronoUnit)),
                    periodLengthMap.get(chronoUnit)
            );
        }
    }

    private void saveTrendBar(final Instant periodStart, final TrendBarPeriod period, final ImmutableList<Quote> quotes) {
        final HashMap<Symbol, TrendBarParam> trendBarParams = Maps.newHashMap();

        for (final Quote quote : quotes) {
            trendBarParams
                    .putIfAbsent(quote.getSymbol(), new TrendBarParam(quote.getSymbol(), period, periodStart));
            trendBarParams
                    .get(quote.getSymbol())
                    .processPriceUpdate(quote.getPrice());
        }
        for (final TrendBarParam trendBarParam : trendBarParams.values()) {
            try {
                trendBarMapper.createTrendBar(
                        trendBarParam.symbol,
                        trendBarParam.period,
                        trendBarParam.openPrice,
                        trendBarParam.closePrice,
                        trendBarParam.highPrice,
                        trendBarParam.lowPrice,
                        Timestamp.from(trendBarParam.startTime)
                );
            } catch (final Exception e) {
                log.error(e);
            }
        }
        log.info(period + " trendbars created");
    }

    private static class TrendBarParam {
        private final Symbol symbol;
        private final TrendBarPeriod period;
        private final Instant startTime;
        private int openPrice = -1;
        private int closePrice;
        private int highPrice;
        private int lowPrice = Integer.MAX_VALUE;

        private TrendBarParam(final Symbol symbol, final TrendBarPeriod period, final Instant startTime) {
            this.symbol = symbol;
            this.period = period;
            this.startTime = startTime;
        }

        private void processPriceUpdate(final int quotePrice) {
            if (openPrice == -1) {
                openPrice = quotePrice;
            }
            highPrice = Math.max(highPrice, quotePrice);
            lowPrice = Math.min(lowPrice, quotePrice);
            closePrice = quotePrice;
        }
    }

    @Override
    public void updateQuote(final Quote quote) {
        checkNotNull(quote, "quote required");

        executorService.submit(() -> {
            log.info("new quote received " + quote.getSymbol() + " -> " + quote.getPrice() + " at " + quote.getTimestamp());

            secondQuotes.put(quote.getTimestamp().truncatedTo(ChronoUnit.SECONDS), quote);
            minuteQuotes.put(quote.getTimestamp().truncatedTo(ChronoUnit.MINUTES), quote);
            hourQuotes.put(quote.getTimestamp().truncatedTo(ChronoUnit.HOURS), quote);
            dayQuotes.put(quote.getTimestamp().truncatedTo(ChronoUnit.DAYS), quote);
        });
    }

    @Override
    public List<TrendBar> listTrendBars(final Symbol symbol,
                                        final TrendBarPeriod period,
                                        final Instant fromTimestamp,
                                        final Instant toTimestamp) {
        checkNotNull(symbol, "symbol required");
        checkNotNull(period, "period required");
        checkNotNull(fromTimestamp, "fromTimestamp required");
        checkNotNull(toTimestamp, "toTimestamp required");

        return trendBarMapper.listTrendBars(symbol, period, Timestamp.from(fromTimestamp), Timestamp.from(toTimestamp));
    }
}
